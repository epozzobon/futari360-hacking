Patches for Mushihimesama Futari for Xbox 360. Affects the 360 versions of both 1.5 and 1.01.

## Changes introduced by the patch

* Counterstop of 3999999999 is removed
* Extra features in training mode:
    * **back+LT** prevents new enemies from spawning
    * **back+RT** makes the game go 16x faster
    * **back+LB** toggles player invincibility
    * **back+RB** disables enemy bullets
* Bullet count, gems count, rank and P1 inputs are displayed on the right

## Installation

The requirements for compiling and installing this patch under windows are:
* [powerpc-eabi-gcc](https://sourceforge.net/projects/mingw-gcc-powerpc-eabi/)
* [xextool](http://xorloser.com/blog/?p=395)
* [cygwin](https://www.cygwin.com/) or [msys](https://www.msys2.org/) with the following packages:
  * [GNU coreutils](https://www.gnu.org/software/coreutils/) for the `md5sum` command
  * [python](https://www.python.org/)
  * [python-pip](https://pypi.org/project/pip/) to install the [intelhex](https://pypi.org/project/intelhex/) module

Before compiling and installing the patch for the first time, create a backup copy of the following files in your Futari directory:

```bash
cp default.xex default.xex.bak
cp media/Module/reco_abnormal.xex media/Module/reco_abnormal.xex.bak
cp media/Module/hirow_abnormal.xex media/Module/hirow_abnormal.xex.bak
```

If all the prerequisites are installed and the backups are created, you should be able to compile and install the patch with the following commands:
```bash
export FUTARI_PATH="/cygdrive/c/xenia/Mushihimesama Futari Ver. 1.5 (RF)/"
make
make install
```

