#pragma once
#include <stdint.h>

// Structures reverse engineered from the original game
typedef struct __attribute__((__packed__)) {
    uint32_t input_scan_idx;
    uint16_t buttons;
    uint8_t left_trigger;
    uint8_t right_trigger;
    int16_t left_analog_x_axis;
    int16_t left_analog_y_axis;
    int16_t right_analog_x_axis;
    int16_t right_analog_y_axis;
} pad_input;

typedef struct {
    float x;
    float y;
    uint32_t c;
} vertex;

typedef struct {
    uint32_t width;
    uint32_t height;
    void *texture;
} texture_obj;

typedef struct {
    texture_obj *texture;
    uint32_t i;
    float w;
    float h;
    float x;
    float y;
    vertex p[4];
} quad_arg;

typedef void (*frame_function)(int, void *);

// functions stolen from futari
void __attribute__((longcall)) draw_quad(void *, quad_arg *);
uint32_t __attribute__((longcall)) get_gamepad_inputs(unsigned gamepad_idx, pad_input *);
void *malloc_aligned(uint32_t size, uint32_t alignment);
void *memcpy(void *dst, void *src, uint32_t size);
int schedule_frame_function(frame_function function,short identifier,void *arg);
void *draw_sprite(uint32_t sprite_idx, uint32_t position_x, uint32_t position_y, uint32_t layer);

// static variables in futari
uint32_t normal_or_replay_or_training[1];
uint32_t p1_invincibility[1];
uint32_t p2_invincibility[1];
uint32_t skip_mode[1];
uint32_t playback_speed[1];
uint32_t disable_enemy_bullets[1];
uint32_t global_rank[1];
uint32_t bullet_count[1];
uint32_t gems_count[1];
uint16_t p1_inputs[1];
