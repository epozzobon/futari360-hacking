#include "futari.h"

static pad_input previous_inputs;

/* This function will be executed by the game scheduler once per frame */
static void patch_frame_function(int closure_id, void* arg) {
    pad_input inputs;
    get_gamepad_inputs(0, &inputs);
    uint32_t buttons_diff = previous_inputs.buttons ^ inputs.buttons;
    previous_inputs = inputs;

    if (*normal_or_replay_or_training != 2) {
        return;
    }
    // Training mode stuff

    if (inputs.buttons & 0x0020) {
        // Back is being held

        if (inputs.buttons & 0x0200) {
            // RB is held
            *disable_enemy_bullets = 1;  // Disable new enemy bullets spawning
        } else {
            *disable_enemy_bullets = 0;  // Enable new enemy bullets spawning
        }
        
        if (inputs.buttons & 0x0100 && buttons_diff & 0x0100) {
            // LB has just been pressed
            if (*p1_invincibility) {
                *p1_invincibility = 0;  // P1 No Invincibility
                *p2_invincibility = 0;  // P2 No Invincibility
            } else {
                *p1_invincibility = 0x7fffffff;  // P1 Invincibility
                *p2_invincibility = 0x7fffffff;  // P2 Invincibility
            }
        }
        
        if (inputs.left_trigger & 0x80) {
            // RL is held
            *skip_mode |= 0x2;  // Disable new enemies spawning
        } else {
            *skip_mode &= ~((uint32_t) 0x2); // Enable new enemies spawning
        }
        
        if (inputs.right_trigger & 0x80) {
            // RT is held
            *playback_speed = 16;  // 16x fast forward
        } else {
            *playback_speed = 1;  // 1x normal speed
        }
    } else {
        if (buttons_diff & 0x0020) {
            // Back was just released, fix all the modifications
            *playback_speed = 1;  // 1x normal speed
            *skip_mode &= ~((uint32_t) 0x2); // Enable new enemies spawning
            *disable_enemy_bullets = 0;  // Enable new enemy bullets spawning
        }
    }
}

/* The instructions injected in startcode.S will jump to this function when the
   game starts */
void game_start_entry() {
    schedule_frame_function(patch_frame_function, 71, (void*) 0);
}

/* Drawing functions */
static void draw_char(float x, float y, char c) {
    // Don't render invisible or missing characters
    if (c <= ' ' || c == '`' || c >= '~') {
        return;
    }

    unsigned d = c - ' ';  // Glyph index in the font
    if (c > '`') {
        // Workaraound for the font lacking '`'
        d--;
    }

    // Some object that needs to be passed to draw_quad
    void *renderer = ((void ***) 0x821f85b0)[0][39];

    // Array of the textures used in the UI
    texture_obj **gui_textures = *(texture_obj ***) 0x40009634;

    // Texture of a somewhat ASCII font, only lacking '`' and '~'
    texture_obj *font_texture = gui_textures[11];
    
    float x0 = x;  // left edge of the destination
    float y0 = y;  // top edge of the destination
    float x1 = x+18;  // right edge of the destination
    float y1 = y-34;  // bottom edge of the destination
    quad_arg q = (quad_arg) {
        .texture = font_texture,
        .i = 0,
        .w = 18, .h = 34,
        .x = (d % 14) * 18,
        .y = (d / 14) * 34,
        .p = {
            (vertex) {x0, y0, 0xffffffff},
            (vertex) {x1, y0, 0xffffffff},
            (vertex) {x1, y1, 0xffffffff},
            (vertex) {x0, y1, 0xffffffff}
        }
    };
    draw_quad(renderer, &q);
}

static void draw_string(float x, float y, char *s) {
    while (*s) {
        draw_char(x, y, *s);
        x += 18;
        s++;
    }
}

static void draw_uint(float x, float y, unsigned n) {
    do {
        draw_char(x, y, '0' + n % 10);
        n /= 10;
        x -= 18;
    } while (n);
}

/* This function will be called once per frame by the patched default.xex,
   using the entries_table array of functions. It is called while the game is
   rendered, so we can render our own raw quads on the screen here, outside of
   the game screen, to amek gadgets. */
void ui_render_entry() {
    // Gadgets
    draw_uint(400, -30, *global_rank);
    draw_string(430, -30, "RANK");
    draw_uint(400,   0, *bullet_count);
    draw_string(430, 0, "BULLETS");
    draw_uint(400,  30, *gems_count);
    draw_string(430, 30, "GEMS");

    // Input display
    uint8_t buttons = (uint8_t) *p1_inputs;
    if (buttons & 4) { draw_char(400, 60, '<'); }
    if (buttons & 1) { draw_char(418, 60, '^'); }
    if (buttons & 2) { draw_char(418, 60, 'v'); }
    if (buttons & 8) { draw_char(436, 60, '>'); }
    if (buttons & 16) { draw_char(472, 60, 'A'); }
    if (buttons & 32) { draw_char(490, 60, 'B'); }
    if (buttons & 64) { draw_char(508, 60, 'C'); }
    if (buttons & 128) { draw_char(526, 60, 'D'); }

#ifdef DEBUG
    // Display one texture per frame
    static unsigned f = 0;
    void *s = ((void **)(((uint32_t *) 0x821f85b0)[0] + 0x9c))[0];
    quad_arg q;
    q.texture = ((void ***) 0x40009634)[0][(f++/64) % 23];
    q.i = 0;
    q.w = 256;
    q.h = 256;
    q.x = 0;
    q.y = 0;
    float x0 = 320;
    float x1 = 576;
    float y0 = -102;
    float y1 = -326;
    q.p[0].x = x0; q.p[0].y = y0; q.p[0].c = 0xffffffff;
    q.p[1].x = x1; q.p[1].y = y0; q.p[1].c = 0xffffffff;
    q.p[2].x = x1; q.p[2].y = y1; q.p[2].c = 0xffffffff;
    q.p[3].x = x0; q.p[3].y = y1; q.p[3].c = 0xffffffff;
    draw_quad(s, &q);
#endif
}
