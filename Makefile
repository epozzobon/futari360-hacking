CC=powerpc-eabi-gcc
LD=powerpc-eabi-ld

CFLAGS=-I. -mcpu=powerpc64 -mpowerpc64 -mbig -mno-toc -mno-eabi -Os --std=c99
LDFLAGS= -nodefaultlibs -nostdlib -static
BUILD_DIR=build

FUTARI_PATH?=Path_To_Mushihimesama_Futari_Directory
UNPATCHED_RECO_ABNORMAL_XEX?=$(FUTARI_PATH)/media/Module/reco_abnormal.xex.bak
RECO_ABNORMAL_XEX_DESTINATION?=$(FUTARI_PATH)/media/Module/reco_abnormal.xex
UNPATCHED_HIROW_ABNORMAL_XEX?=$(FUTARI_PATH)/media/Module/hirow_abnormal.xex.bak
HIROW_ABNORMAL_XEX_DESTINATION?=$(FUTARI_PATH)/media/Module/hirow_abnormal.xex
UNPATCHED_DEFAULT_XEX?=$(FUTARI_PATH)/default.xex.bak
DEFAULT_XEX_DESTINATION?=$(FUTARI_PATH)/default.xex

.PHONY: install clean disasm all test
all: $(BUILD_DIR) \
	$(BUILD_DIR)/patch.reco_abnormal.elf $(BUILD_DIR)/patch.reco_abnormal.hex \
	$(BUILD_DIR)/patch.reco_abnormal.map \
	$(BUILD_DIR)/reco_abnormal.xex \
	$(BUILD_DIR)/patch.hirow_abnormal.elf $(BUILD_DIR)/patch.hirow_abnormal.hex \
	$(BUILD_DIR)/patch.hirow_abnormal.map \
	$(BUILD_DIR)/hirow_abnormal.xex \
	$(BUILD_DIR)/default.xex

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/training_patch.o: training_patch.c futari.h
	$(CC) -c -o $@ training_patch.c $(CFLAGS)

$(BUILD_DIR)/savegpr.o: savegpr.S
	$(CC) -c -o $@ savegpr.S $(CFLAGS)



$(BUILD_DIR)/default_entry.o: default_entry.S
	$(CC) -c -o $@ default_entry.S $(CFLAGS)
	
$(BUILD_DIR)/default_patch.o: default_patch.c
	$(CC) -c -o $@ default_patch.c $(CFLAGS)

$(BUILD_DIR)/patch.default.elf $(BUILD_DIR)/patch.default.map: default.ld $(BUILD_DIR)/default_entry.o $(BUILD_DIR)/savegpr.o $(BUILD_DIR)/default_patch.o
	$(LD) -T default.ld -o $(BUILD_DIR)/patch.default.elf $(BUILD_DIR)/default_entry.o $(BUILD_DIR)/savegpr.o $(BUILD_DIR)/default_patch.o -Map=$(BUILD_DIR)/patch.default.map 

$(BUILD_DIR)/patch.default.hex: $(BUILD_DIR)/patch.default.elf
	powerpc-eabi-objcopy -O ihex $(BUILD_DIR)/patch.default.elf $(BUILD_DIR)/patch.default.hex

$(BUILD_DIR)/default.xex: $(BUILD_DIR)/patch.default.hex
ifneq "$(shell md5sum '$(UNPATCHED_DEFAULT_XEX)' | cut -d' ' -f 1)" "46a2e23336131dfffe93db134f8640de"
	echo "Unexpeced xex file, please check unpatched default.xex path"
	exit 1
endif
	cp "$(UNPATCHED_DEFAULT_XEX)" $(BUILD_DIR)/default.xex.tmp
	# Uncompress and decrypt default.xex
	xextool -c u -e u $(BUILD_DIR)/default.xex.tmp
	python patch.py --rn 0x81FFE000 "$(BUILD_DIR)/patch.default.hex" "$(BUILD_DIR)/default.xex.tmp" "$(BUILD_DIR)/default.xex"



$(BUILD_DIR)/startcode.reco_abnormal.o: startcode.S
	$(CC) -c -o $@ startcode.S $(CFLAGS) -DRECO_ABNORMAL

$(BUILD_DIR)/patch.reco_abnormal.elf $(BUILD_DIR)/patch.reco_abnormal.map: reco_abnormal.ld $(BUILD_DIR)/startcode.reco_abnormal.o $(BUILD_DIR)/savegpr.o $(BUILD_DIR)/training_patch.o 
	$(LD) -T reco_abnormal.ld -o $(BUILD_DIR)/patch.reco_abnormal.elf $(BUILD_DIR)/startcode.reco_abnormal.o $(BUILD_DIR)/savegpr.o $(BUILD_DIR)/training_patch.o -Map=$(BUILD_DIR)/patch.reco_abnormal.map 

$(BUILD_DIR)/patch.reco_abnormal.hex: $(BUILD_DIR)/patch.reco_abnormal.elf
	powerpc-eabi-objcopy -O ihex $(BUILD_DIR)/patch.reco_abnormal.elf $(BUILD_DIR)/patch.reco_abnormal.hex

$(BUILD_DIR)/reco_abnormal.xex: $(BUILD_DIR)/patch.reco_abnormal.hex
ifneq "$(shell md5sum '$(UNPATCHED_RECO_ABNORMAL_XEX)' | cut -d' ' -f 1)" "62788e41f3e1c83957fe86f1bf94031e"
	echo "Unexpeced xex file, please check unpatched reco_abnormal.xex path"
	exit 1
endif
	cp "$(UNPATCHED_RECO_ABNORMAL_XEX)" $(BUILD_DIR)/reco_abnormal.xex.tmp
	# Uncompress and decrypt reco_abnormal.xex
	xextool -c u -e u $(BUILD_DIR)/reco_abnormal.xex.tmp
	python patch.py --rn 0x87FFE000 "$(BUILD_DIR)/patch.reco_abnormal.hex" "$(BUILD_DIR)/reco_abnormal.xex.tmp" "$(BUILD_DIR)/reco_abnormal.xex"



$(BUILD_DIR)/startcode.hirow_abnormal.o: startcode.S
	$(CC) -c -o $@ startcode.S $(CFLAGS) -DHIROW_ABNORMAL

$(BUILD_DIR)/patch.hirow_abnormal.hex: $(BUILD_DIR)/patch.hirow_abnormal.elf
	powerpc-eabi-objcopy -O ihex $(BUILD_DIR)/patch.hirow_abnormal.elf $(BUILD_DIR)/patch.hirow_abnormal.hex

$(BUILD_DIR)/patch.hirow_abnormal.elf $(BUILD_DIR)/patch.hirow_abnormal.map: hirow_abnormal.ld $(BUILD_DIR)/startcode.hirow_abnormal.o $(BUILD_DIR)/savegpr.o $(BUILD_DIR)/training_patch.o 
	$(LD) -T hirow_abnormal.ld -o $(BUILD_DIR)/patch.hirow_abnormal.elf $(BUILD_DIR)/startcode.hirow_abnormal.o $(BUILD_DIR)/savegpr.o $(BUILD_DIR)/training_patch.o -Map=$(BUILD_DIR)/patch.hirow_abnormal.map

$(BUILD_DIR)/hirow_abnormal.xex: $(BUILD_DIR)/patch.hirow_abnormal.hex
ifneq "$(shell md5sum '$(UNPATCHED_HIROW_ABNORMAL_XEX)' | cut -d' ' -f 1)" "ddcdfccb16b44e24b2092f61fba9b4c3"
	echo "Unexpeced xex file, please check unpatched hirow_abnormal.xex path"
	exit 1
endif
	cp "$(UNPATCHED_HIROW_ABNORMAL_XEX)" $(BUILD_DIR)/hirow_abnormal.xex.tmp
	# Uncompress and decrypt hirow_abnormal.xex
	xextool -c u -e u $(BUILD_DIR)/hirow_abnormal.xex.tmp
	python patch.py --rn 0x87FFE000 "$(BUILD_DIR)/patch.hirow_abnormal.hex" "$(BUILD_DIR)/hirow_abnormal.xex.tmp" "$(BUILD_DIR)/hirow_abnormal.xex"

install: $(BUILD_DIR)/reco_abnormal.xex $(BUILD_DIR)/hirow_abnormal.xex $(BUILD_DIR)/default.xex
	cp $(BUILD_DIR)/reco_abnormal.xex "$(RECO_ABNORMAL_XEX_DESTINATION)"
	cp $(BUILD_DIR)/hirow_abnormal.xex "$(HIROW_ABNORMAL_XEX_DESTINATION)"
	cp $(BUILD_DIR)/default.xex "$(DEFAULT_XEX_DESTINATION)"

clean:
	rm -rf $(BUILD_DIR)/*

disasm: $(BUILD_DIR)/patch.reco_abnormal.elf $(BUILD_DIR)/patch.default.elf
	powerpc-eabi-objdump --disassemble-all $(BUILD_DIR)/patch.default.elf

test:
	xenia "$(DEFAULT_XEX_DESTINATION)"