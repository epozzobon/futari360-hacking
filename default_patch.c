/* C functions to be added to the default.xex file (the main wrapper) */

#include <stdint.h>

typedef void (*fun_ptr)(void);
fun_ptr entries_table[4];
void *fingerprint[1];

void once_per_frame() {
    // Check if the currently running game is patched
    if (((uint32_t *) fingerprint)[5] == 0x706f7a7a) {
        // If the running game is patched, then it will have this entry table
        (entries_table[0])();
    }
}
