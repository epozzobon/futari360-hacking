/* Entry code to be injected into the default.xex file (the main wrapper) */

.extern     once_per_frame
.section    .jump_to_once_per_frame , "ax"
	// We are replacing a stub, empty function so there is nothing to preserve
	// or restore.
	b       once_per_frame