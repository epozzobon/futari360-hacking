#!/usr/bin/env python3

from intelhex import IntelHex
import sys
from shutil import copyfile
import argparse


# Simple python script to apply a hex file as a patch on top of a binary file

if __name__ == '__main__':

    parser = argparse.ArgumentParser(prog='patch.py', add_help=False)
    group1 = parser.add_mutually_exclusive_group()
    group1.add_argument('--r', help='Relocation of hex addresses', nargs=1,
                        type=lambda x: hex(int(x, 0)))
    group1.add_argument('--rn', help='Negative relocation of hex addresses',
                        nargs=1, type=lambda x: hex(int(x, 0)))
    parser.add_argument('patch_hex_path', help='Path of input patch hex')
    parser.add_argument('input_bin_path', help='Path of input bin')
    parser.add_argument('output_bin_path', help='Path of output bin')
    args = parser.parse_args(sys.argv[1:])

    if args.r is not None:
        relocate = int(args.r[0], 0)
    elif args.rn is not None:
        relocate = -int(args.rn[0], 0)
    else:
        relocate = 0

    # Patch a binary file
    patch_hex_path = args.patch_hex_path
    input_bin_path = args.input_bin_path
    output_bin_path = args.output_bin_path

    copyfile(input_bin_path, output_bin_path)
    ih = IntelHex(patch_hex_path)
    with open(output_bin_path, 'r+b') as fout:
        for sstart, sstop in ih.segments():
            b = ih.gets(sstart, sstop - sstart)
            off = sstart + relocate
            print("seeking at 0x%x to write %d bytes" % (off, len(b)))
            fout.seek(off, 0)
            fout.write(b)
